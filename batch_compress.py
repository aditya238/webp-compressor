import os
from webp_util import *
import multiprocessing as mp
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--max_size", default=100)
parser.add_argument("--frame_skip", default=0)
parser.add_argument("--quality", default=50)
parser.add_argument("-m", "--method", default="LANCZOS", choices=methods.keys())
parser.add_argument("-s", "--source", default="./stickers/")
parser.add_argument("-o", "--output", default="./compressed/")
args = parser.parse_args()

if __name__ == "__main__":
    max_size = int(args.max_size)  # Img size
    frame_skip = int(args.frame_skip)  # Number of frames to skip
    quality = int(args.quality)
    method = args.method

    in_directory = args.source.rstrip("/") + "/"
    out_directory = args.output.rstrip("/") + "/"
    out_directory = f"{out_directory}/{max_size}_q{quality}_fs{frame_skip}/"

    if not os.path.exists(out_directory):
        os.makedirs(out_directory)

    # Get a list of all files in the directory
    file_list = os.listdir(in_directory)


    def process(file_name):
        try:
            image = load_webp(in_directory + file_name)
            output_path = out_directory + file_name
            compress_webp(image, output_path, frame_skip, max_size, quality, methods[method])
            print(f"Compressed {file_name} successfully")
        except Exception as e:
            print(f"Failed to load {file_name}")

    pool = mp.Pool(mp.cpu_count())
    pool.map(process, file_list)
    pool.close()
